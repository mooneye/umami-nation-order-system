<?php

namespace UmamiNationBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Class DefaultsSettingFormFactory
 * @package UmamiNationBundle\Form
 *
 * Makes forms with the standard SF2 Form Factory and sets default values
 */
class DefaultsSettingFormFactory implements FormFactoryInterface
{
    /**
     * @var FormFactoryInterface
     */
    private $factory;

    /**
     * @var DefaultSettingsResolver
     */
    private $resolver;

    /**
     * DefaultsSettingFormFactory constructor.
     * @param FormFactoryInterface $factory
     * @param DefaultSettingsResolver $resolver
     */
    public function __construct(FormFactoryInterface $factory, DefaultSettingsResolver $resolver)
    {
        $this->resolver = $resolver;
        $this->factory = $factory;
    }

    /**
     * {@inheritdoc}
     */
    public function create(
        $type = 'form',
        $data = null,
        array $options = []
    ): FormInterface {
        if ($data === null) {
            $data = $this->resolver->getDefaultsForForm($type);
        }

        return $this->factory->create($type, $data, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function createNamed($name, $type = 'form', $data = null, array $options = array()): FormInterface
    {
        return $this->factory->createNamed($name, $type, $data, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function createForProperty($class, $property, $data = null, array $options = array()): FormInterface
    {
        return $this->factory->createForProperty($class, $property, $data, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function createBuilder(
        $type = 'form',
        $data = null,
        array $options = []
    ): FormBuilderInterface {
        return $this->factory->createBuilder($type, $data, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function createNamedBuilder(
        $name,
        $type = 'form',
        $data = null,
        array $options = []
    ): FormBuilderInterface {
        return $this->factory->createNamedBuilder($name, $type, $data, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function createBuilderForProperty(
        $class,
        $property,
        $data = null,
        array $options = []
    ): FormBuilderInterface {
        return $this->factory->createBuilderForProperty($class, $property, $data, $options);
    }
}
