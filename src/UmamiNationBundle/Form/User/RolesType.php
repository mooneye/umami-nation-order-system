<?php

namespace UmamiNationBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

/**
 * Class RolesType
 * @package UmamiNationBundle\Form\User
 */
class RolesType extends AbstractType
{
    /** @var  Role[] */
    protected $reachableRoles;
    /** @var Role[]  */
    protected $roleChoices;
    /** @var string  */
    private $transPrefix = 'umami-nation.security.roles.';

    /**
     * RolesType constructor.
     * @param RoleHierarchy $roleHierarchy
     * @param AuthorizationChecker $authorizationChecker
     */
    public function __construct(RoleHierarchy $roleHierarchy, AuthorizationChecker $authorizationChecker)
    {
        $superAdminRole = 'ROLE_SUPER_ADMIN';
        $currentPermissions = [new Role($superAdminRole)];
        $this->reachableRoles = $roleHierarchy->getReachableRoles($currentPermissions);
        $this->roleChoices = $this->getRoleChoices();
        if (!$authorizationChecker->isGranted($superAdminRole)) {
            unset($this->roleChoices[$this->transPrefix.$superAdminRole]);
        }
    }

    /**
     * @param OptionsResolver $resolver
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => $this->getRoleChoices(),
            'choice_translation_domain' => true,
            'expanded' => false,
            'multiple' => false
        ]);
    }

    /**
     * @return Role[]
     */
    private function getRoleChoices()
    {
        $roles = [];
        /** @var Role $role**/
        foreach ($this->reachableRoles as $role) {
            $roleStr = $role->getRole();
            $roles[$this->transPrefix.$roleStr] = $roleStr;
        }

        return $roles;
    }

    /**
     * @return null|string
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
