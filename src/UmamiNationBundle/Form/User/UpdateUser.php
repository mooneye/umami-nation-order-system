<?php
namespace UmamiNationBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UpdateUser
 * @package UmamiNationBundle\Form\User
 */
class UpdateUser extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('post')
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'umami-nation.user.form.label.name',
                    'required' => true,
                    'attr' => [
                        'placeholder' => 'umami-nation.user.form.placeholder.username'
                    ]
                ]
            )
            ->add(
                'firstName',
                TextType::class,
                [
                    'label' => 'umami-nation.user.form.label.firstName',
                    'attr' => [
                        'placeholder' => 'umami-nation.user.form.placeholder.firstName'
                    ]
                ]
            )
            ->add(
                'lastName',
                TextType::class,
                [
                    'label' => 'umami-nation.user.form.label.lastName',
                    'attr' => [
                        'placeholder' => 'umami-nation.user.form.placeholder.lastName'
                    ]
                ]
            )
            ->add(
                'password',
                PasswordType::class,
                [
                    'label' => 'umami-nation.user.form.label.password',
                    'required' => true,
                    'attr' => [
                        'pattern' => '.{6,}',
                        'title' => 'minimum of 6 characters',
                        'placeholder' => 'umami-nation.user.form.placeholder.password'
                    ]
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'umami-nation.user.form.label.email',
                    'required' => true,
                    'attr' => [
                        'placeholder' => 'umami-nation.user.form.placeholder.email'
                    ]
                ]
            )
            ->add(
                'locale',
                LocalesType::class,
                [
                    'label' => 'umami-nation.user.form.label.language',
                    'required' => false,
                    'placeholder' => 'umami-nation.user.form.placeholder.language'
                ]
            )
            ->add(
                'roles',
                RolesType::class,
                [
                    'label' => 'umami-nation.security.form.label.role',
                    'required' => false,
                    'placeholder' => 'umami-nation.user.form.placeholder.role',
                    'choice_value' => function ($role) {
                        if (\is_array($role)) {
                            return $role[0];
                        }

                        return $role;
                    }
                ]
            )
            ->add(
                'isActive',
                CheckboxType::class,
                [
                    'label' => 'umami-nation.user.form.label.active_status',
                    'required' => false,
                    'data' => true
                ]
            );
    }

    /**
     * {@inheritdoc}
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'saveButtonText' => 'umami-nation.user.action.update'
            ]
        );
    }
}
