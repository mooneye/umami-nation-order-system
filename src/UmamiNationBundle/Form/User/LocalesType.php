<?php

namespace UmamiNationBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Role\Role;

/**
 * Class LocalesType
 * @package UmamiNationBundle\Form\User
 */
class LocalesType extends AbstractType
{
    /** @var  Role[] */
    protected $locales;

    /**
     * LocalesType constructor.
     * @param $locales
     */
    public function __construct($locales)
    {
        $this->locales = $locales;
    }

    /**
     * @param OptionsResolver $resolver
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => $this->getLocaleChoices()
        ]);
    }

    /**
     * @return Role[]
     */
    private function getLocaleChoices()
    {
        $locales = [];
        foreach ($this->locales as $locale => $value) {
            $locales[(string)$value] = $locale;
        }

        return $locales;
    }

    /**
     * @return null|string
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
