<?php

namespace UmamiNationBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class SearchUser
 * @package UmamiNationBundle\Form\User
 */
class SearchUser extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('get')
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'umami-nation.user.form.label.name',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'umami-nation.user.form.placeholder.username'
                    ]
                ]
            )
            ->add(
                'email',
                NumberType::class,
                [
                    'label' => 'umami-nation.user.form.label.email',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'umami-nation.user.form.placeholder.email'
                    ]
                ]
            )
            ->add(
                'firstName',
                TextType::class,
                [
                    'label' => 'umami-nation.user.form.label.firstName',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'umami-nation.user.form.placeholder.firstName'
                    ]
                ]
            )->add(
                'lastName',
                TextType::class,
                [
                    'label' => 'umami-nation.user.form.label.lastName',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'umami-nation.user.form.placeholder.lastName'
                    ]
                ]
            )->add(
                'roles',
                RolesType::class,
                [
                    'label' => 'umami-nation.security.form.label.role',
                    'required' => false,
                    'placeholder' => 'umami-nation.user.form.placeholder.role'
                ]
            )
            ->add(
                'isActive',
                CheckboxType::class,
                [
                    'label' => 'umami-nation.user.form.label.only_active',
                    'required' => false,
                    'data' => true
                ]
            );
    }
}
