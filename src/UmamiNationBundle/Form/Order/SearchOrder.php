<?php

namespace UmamiNationBundle\Form\Order;

use UmamiNationBundle\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\Exception\InvalidArgumentException;

/**
 * Class SearchOrder
 * @package UmamiNationBundle\Form\Order
 */
class SearchOrder extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws InvalidArgumentException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('get')
            ->add(
                'product',
                EntityType::class,
                [
                    'class' => Product::class,
                    'label' => 'umami-nation.order.form.label.product',
                    'placeholder' => 'umami-nation.order.form.placeholder.product',
                    'choice_label' => 'name',
                    'required' => false
                ]
            )
            ->add(
                'orderEnd',
                DateTimeType::class,
                [
                    'label' => 'umami-nation.order.form.label.order_from',
                    'required' => false,
                    'placeholder' => [
                        'year' => 'umami-nation.order.form.placeholder.year',
                        'month' => 'umami-nation.order.form.placeholder.month',
                        'day' => 'umami-nation.order.form.placeholder.day',
                        'hour' => 'umami-nation.order.form.placeholder.hour',
                        'minute' => 'umami-nation.order.form.placeholder.minute'
                    ]
                ]
            );
    }
}
