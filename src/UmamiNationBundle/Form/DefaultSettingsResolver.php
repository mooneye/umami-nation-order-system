<?php

namespace UmamiNationBundle\Form;

/**
 * Class DefaultSettingsResolver
 * @package UmamiNationBundle\Form
 *
 * Finds Defaults Configuration for a specific Form Type in the given configuration
 *
 */
class DefaultSettingsResolver
{
    /**
     * @var array  assoc array with default values for all forms
     */
    private $defaults;

    /**
     * DefaultSettingsResolver constructor.
     * @param array $defaults
     *
     * Takes Default Settings Array from Configuration (config.yaml)
     * The config has to be structures like the namespaces inside the Form directory of
     * the bundle:
     * The subarray ["Port"]["SearchForm"] will be used for Type UmamiNationBundle\Form\Port\SearchForm.
     */
    public function __construct(array $defaults)
    {
        $this->defaults = $defaults;
    }

    /**
     * @param $type
     * @return array
     *
     * Tries to find default settings for a given form inside the default settings array.
     *
     * If settings for this form are found, the resulting sub-array is returned.
     */
    public function getDefaultsForForm($type): array
    {
        if (\is_object($type)) {
            $type = \get_class($type);
        }

        $type = \str_replace('UmamiNationBundle\\Form\\', '', $type);
        $path = \explode('\\', $type);
        $defaults = $this->defaults;
        foreach ($path as $pathItem) {
            if (!\array_key_exists($pathItem, $defaults)) {
                return [];
            }

            $defaults = $defaults[$pathItem];
        }

        return $defaults;
    }
}
