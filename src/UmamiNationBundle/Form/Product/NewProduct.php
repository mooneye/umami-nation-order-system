<?php

namespace UmamiNationBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class NewProduct
 * @package UmamiNationBundle\Form\Product
 */
class NewProduct extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('post')
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'umami-nation.product.form.label.name',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'umami-nation.product.form.placeholder.name'
                    ]
                ]
            )
            ->add(
                'price',
                NumberType::class,
                [
                    'label' => 'umami-nation.product.form.label.price',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'umami-nation.product.form.placeholder.price'
                    ]
                ]
            )
            ->add(
                'recipe',
                TextareaType::class,
                [
                    'label' => 'umami-nation.product.form.label.recipe',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'umami-nation.product.form.placeholder.recipe'
                    ]
                ]
            )
            ->add(
                'duration',
                NumberType::class,
                [
                    'label' => 'umami-nation.product.form.label.duration',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'umami-nation.product.form.placeholder.duration'
                    ]
                ]
            );
    }
}
