<?php

namespace UmamiNationBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class SearchProduct
 * @package UmamiNationBundle\Form\Product
 */
class SearchProduct extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('get')
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'umami-nation.product.form.label.name',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'umami-nation.product.form.placeholder.name'
                    ]
                ]
            )
            ->add(
                'price',
                NumberType::class,
                [
                    'label' => 'umami-nation.product.form.label.price',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'umami-nation.product.form.placeholder.price'
                    ]
                ]
            );
    }
}
