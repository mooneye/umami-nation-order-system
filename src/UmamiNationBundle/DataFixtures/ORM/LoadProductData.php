<?php

namespace UmamiNationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use UmamiNationBundle\Entity\Product;

/**
 * Class LoadProductData
 * @package UmamiNationBundle\DataFixtures\ORM
 */
class LoadProductData implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritDoc}
     * @throws ServiceCircularReferenceException
     * @throws ServiceNotFoundException
     * @throws InvalidArgumentException
     */
    public function load(ObjectManager $manager)
    {
        $dataProvider = $this->container->get('umami-nation.fixtures.provider.data');
        foreach ($dataProvider->getFixturesByName('products') as $entry) {
            $product = new Product();
            $product->setName($entry['name']);
            $product->setPrice($entry['price']);
            $product->setRecipe($entry['recipe']);
            $product->setDuration($entry['duration']);
            $manager->persist($product);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     * @return integer
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function getOrder(): int
    {
        return $this->container->get('umami-nation.fixtures.provider.order')
            ->getOrder(static::class);
    }
}
