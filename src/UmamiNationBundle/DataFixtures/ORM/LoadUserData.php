<?php

namespace UmamiNationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use UmamiNationBundle\Entity\User;

/**
 * Class LoadUserData
 * @package UmamiNationBundle\DataFixtures\ORM
 */
class LoadUserData implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritDoc}
     * @throws ServiceCircularReferenceException
     * @throws ServiceNotFoundException
     * @throws InvalidArgumentException
     */
    public function load(ObjectManager $manager)
    {
        $dataProvider = $this->container->get('umami-nation.fixtures.provider.data');
        foreach ($dataProvider->getFixturesByName('users') as $entry) {
            $user = new User();
            $user->setUsername($entry['username']);
            $user->setEmail($entry['email']);
            $user->setFirstName($entry['firstName']);
            $user->setLastName($entry['lastName']);
            $user->setRoles($entry['role']);
            $user->setLocale($entry['lang']);
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $entry['pw']);
            $user->setPassword($encoded);

            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     * @return integer
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function getOrder(): int
    {
        return $this->container->get('umami-nation.fixtures.provider.order')
            ->getOrder(static::class);
    }
}
