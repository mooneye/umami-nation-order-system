<?php

namespace UmamiNationBundle\DataFixtures\Provider;

/**
 * Class Order
 * @package UmamiNationBundle\DataFixtures\Provider
 */
class Order
{
    /**
     * @var array
     */
    private $fixturesOrder;

    /**
     * Order constructor.
     * @param array $fixturesOrder
     * @throws \InvalidArgumentException
     */
    public function __construct(array $fixturesOrder)
    {
        $this->fixturesOrder = $fixturesOrder;
    }

    /**
     * @param string $className
     * @return int
     */
    public function getOrder($className): int
    {
        $order = \array_search(
            $this->getReducedClassName($className),
            $this->getFixturesOrder(),
            true
        );

        return $order ? $order + 1 : 1;
    }

    /**
     * @return array
     */
    public function getFixturesOrder(): array
    {
        return $this->fixturesOrder;
    }

    /**
     * @param string $className
     * @return bool|int|string
     */
    private function getReducedClassName($className)
    {
        if ($pos = strrpos($className, '\\')) {
            return substr($className, $pos + 1);
        }
        return $pos;
    }
}
