<?php

namespace UmamiNationBundle\DataFixtures\Provider;

/**
 * Class Order
 * @package UmamiNationBundle\DataFixtures\Provider
 */
class Data
{
    /**
     * @var array
     */
    private $fixtures;

    /**
     * Order constructor.
     * @param array $fixtures
     * @throws \InvalidArgumentException
     */
    public function __construct(array $fixtures)
    {
        $this->fixtures = $fixtures;
    }

    /**
     * @return array
     */
    public function getFixtures(): array
    {
        return $this->fixtures;
    }

    /**
     * @param string $name
     * @return array
     */
    public function getFixturesByName($name): array
    {
        return \array_key_exists($name, $this->fixtures) ? $this->fixtures[$name] : [];
    }
}
