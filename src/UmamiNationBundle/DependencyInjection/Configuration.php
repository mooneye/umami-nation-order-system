<?php
namespace UmamiNationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package UmamiNationBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     * @throws \RuntimeException
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder;
        $rootNode = $treeBuilder->root('umami_nation');
        $rootNode
            ->children()
                ->arrayNode('fixtures_order')
                    ->prototype('scalar')->end()
                ->end()
                ->arrayNode('fixtures')
                    ->children()
                        ->append($this->getProducts())
                        ->append($this->getUsers())
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

    /**
     * @return mixed
     * @throws \RuntimeException
     */
    private function getProducts()
    {
        $builder = new TreeBuilder();
        $node = $builder->root('products');

        return $node
            ->prototype('array')
                ->children()
                    ->scalarNode('name')->isRequired()->end()
                    ->scalarNode('price')->isRequired()->end()
                    ->scalarNode('recipe')->isRequired()->end()
                    ->scalarNode('duration')->isRequired()->end()
                ->end()
            ->end();
    }

    /**
     * @return mixed
     * @throws \RuntimeException
     */
    private function getUsers()
    {
        $builder = new TreeBuilder();
        $node = $builder->root('users');

        return $node
            ->prototype('array')
                ->children()
                    ->scalarNode('username')->isRequired()->end()
                    ->scalarNode('email')->isRequired()->end()
                    ->scalarNode('firstName')->isRequired()->end()
                    ->scalarNode('lastName')->isRequired()->end()
                    ->scalarNode('role')->isRequired()->end()
                    ->scalarNode('lang')->isRequired()->end()
                    ->scalarNode('pw')->isRequired()->end()
                ->end()
            ->end();
    }
}
