<?php
namespace UmamiNationBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class UmamiNationExtension
 * @package UmamiNationBundle\DependencyInjection
 */
class UmamiNationExtension extends Extension
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     * @throws \Symfony\Component\DependencyInjection\Exception\OutOfBoundsException
     * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
     */
    public function load(array $config, ContainerBuilder $container)
    {
        $configuration = $this->getConfiguration($config, $container);
        $config = $this->processConfiguration($configuration, $config);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(\dirname(__DIR__) . '/Resources/config'));
        $loader->load('services.yml');

        $container->getDefinition('umami-nation.fixtures.provider.order')
            ->replaceArgument(0, $config['fixtures_order']);

        $container->getDefinition('umami-nation.fixtures.provider.data')
            ->replaceArgument(0, $config['fixtures']);
    }
}
