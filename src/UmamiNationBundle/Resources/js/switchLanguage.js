(function (switchLanguage, $) {
  'use strict';

  switchLanguage.checkChangedLanguage = function (currentLanguage) {
    var newLanguage = $('.language-form a').attr('data-value');
    $("input[name='lang']").val(newLanguage);

    if (newLanguage === currentLanguage) {
      // umamiNotify.errorMessage("Language already set");
    } else {
      // umamiNotify.infoMessage("Setting new language...");
      $('form.language-form').submit();
    }
  };
})(window.switchLanguage = window.switchLanguage || {}, window.jQuery);
