<?php

namespace UmamiNationBundle\Kitchen;

use UmamiNationBundle\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;

class Kitchenmanager
{
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var Paginator */
    private $knpPaginator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param Paginator $knpPaginator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Paginator $knpPaginator
    ) {
        $this->entityManager = $entityManager;
        $this->knpPaginator = $knpPaginator;
    }

    /**
     * @param array $criteria
     *
     * @return array|Order[]
     * @throws \UnexpectedValueException
     */
    public function findBy(array $criteria)
    {
        return $this->entityManager->getRepository(Order::class)->findBy($criteria);
    }

    /**
     * @return array|Order[]
     */
    public function findAll()
    {
        return $this->entityManager->getRepository(Order::class)->findAll();
    }

    /**
     * @param $currentPage
     * @return Order[]
     */
    public function findOrdersNotReady($currentPage)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Order::class)->createQueryBuilder('o');
        $query = $qb->select('o')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->isNull('o.kitchenStart'),
                    $qb->expr()->isNull('o.kitchenEnd'),
                    $qb->expr()->isNull('o.orderEnd')
                )
            )
            ->orderBy('o.orderStart', 'ASC')
            ->getQuery();

        return $this->paginate($query, $currentPage);
    }

    /**
     * @param $query
     * @param $page
     * @return PaginationInterface
     * @throws \LogicException
     */
    private function paginate($query, $page)
    {
        return $this->knpPaginator->paginate(
            $query,
            $page
        );
    }

    /**
     * @param Order $order
     * @throws ORMInvalidArgumentException
     * @throws OptimisticLockException
     */
    public function start(Order $order)
    {
        $order->setKitchenStart(new \DateTime('now'));
        $this->save($order);
    }

    /**
    * @param Order $order
    * @throws ORMInvalidArgumentException
    * @throws OptimisticLockException
    */
    public function save(Order $order)
    {
        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    /**
     * @param $currentPage
     * @return PaginationInterface
     * @throws \LogicException
     */
    public function findOrdersRunning($currentPage)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Order::class)->createQueryBuilder('o');
        $query = $qb->select('o')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->isNotNull('o.kitchenStart'),
                    $qb->expr()->isNull('o.kitchenEnd'),
                    $qb->expr()->isNull('o.orderEnd')
                )
            )
            ->orderBy('o.kitchenStart', 'ASC')
            ->getQuery();

        return $this->paginate($query, $currentPage);
    }

    /**
     * @param Order $order
     * @throws ORMInvalidArgumentException
     * @throws OptimisticLockException
     */
    public function end(Order $order)
    {
        $order->setKitchenEnd(new \DateTime('now'));
        $this->save($order);
    }

    public function findOpenOrders($currentPage)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Order::class)->createQueryBuilder('o');
        $query = $qb->select('o')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->isNull('o.kitchenEnd'),
                    $qb->expr()->isNull('o.orderEnd')
                )
            )
            ->orderBy('o.orderStart', 'ASC')
            ->orderBy('o.kitchenStart', 'ASC')
            ->getQuery();

        return $this->paginate($query, $currentPage);
    }
}
