<?php

namespace UmamiNationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class UmamiNationBundle
 * @package UmamiNationBundle
 */
class UmamiNationBundle extends Bundle
{
}
