<?php

namespace UmamiNationBundle\Controller;

use UmamiNationBundle\Archive\ArchiveManager;
use UmamiNationBundle\Entity\Order;
use UmamiNationBundle\Form\Order\SearchOrder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;

/**
 * @Route(service="umami-nation.controller.archive")
 */
class ArchiveController
{
    /** @var ArchiveManager */
    private $archiveManager;
    /** @var FormFactoryInterface  */
    private $formFactory;

    /**
     * ArchiveController constructor.
     * @param ArchiveManager $archiveManager
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(ArchiveManager $archiveManager, FormFactoryInterface $formFactory)
    {
        $this->archiveManager = $archiveManager;
        $this->formFactory = $formFactory;
    }

    /**
     * @Route("/archive", name="archive")
     * @Template()
     * @Method("GET")
     * @Security("has_role('ROLE_USER')")
     * @param Request $request
     * @return Order[]
     * @throws InvalidOptionsException
     * @throws \UnexpectedValueException
     * @throws \LogicException
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page') ?: 1;
        $searchForm = $this->createSearchForm();
        $searchForm->handleRequest($request);

        $pagination = [];
        $formSubmitted = $searchForm->isSubmitted();
        if ($formSubmitted && $searchForm->isValid()) {
            $pagination = $this->archiveManager->search($searchForm->getData(), $page);
        }

        return [
            'pagination' => $pagination,
            'formSubmitted' => $formSubmitted,
            'search_form' => $searchForm->createView()
        ];
    }

    /**
     * @return FormInterface
     * @throws InvalidOptionsException
     * @throws InvalidOptionsException
     */
    private function createSearchForm(): FormInterface
    {
        return $this->formFactory->create(
            SearchOrder::class
        );
    }
}
