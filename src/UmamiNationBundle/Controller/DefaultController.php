<?php

namespace UmamiNationBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;

/**
 * @Route(service="umami-nation.controller.demo_controller")
 */
class DefaultController
{
    /**
     * @var Translator
     */
    private $translator;

    /**
     * DefaultController constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/", name="homepage")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request): array
    {
        return [
            'translator' => $this->translator
        ];
    }
}
