<?php

namespace UmamiNationBundle\Controller;

use UmamiNationBundle\Entity\Order;
use UmamiNationBundle\Order\OrderManager;
use UmamiNationBundle\Product\ProductManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * @Route(service="umami-nation.controller.order")
 */
class OrderController
{
    /** @var ProductManager */
    private $productManager;
    /** @var Router */
    private $router;
    /** @var  OrderManager */
    private $orderManager;

    /**
     * OrderController constructor.
     * @param ProductManager $manager
     * @param Router $router
     * @param OrderManager $orderManager
     */
    public function __construct(Router $router, ProductManager $manager, OrderManager $orderManager)
    {
        $this->router = $router;
        $this->productManager = $manager;
        $this->orderManager = $orderManager;
    }

    /**
     * @Route("/orders", name="orders")
     * @Template()
     * @Method("GET")
     * @Security("has_role('ROLE_USER')")
     * @param Request $request
     * @return Product[]|array
     * @throws \LogicException
     */
    public function indexAction(Request $request): array
    {
        $products = $this->productManager->findAll();

        $page = $request->query->get('page') ?: 1;
        $pagination = $this->orderManager->findReadyForCheckOut($page);

        return [
            'products' => $products,
            'pagination' => $pagination
        ];
    }

    /**
     * @Route("/order/add/{id}", name="order_add")
     * @Method({"GET", "POST"})
     * @ParamConverter("product", class="UmamiNationBundle\Entity\Product")
     * @param $product
     * @return RedirectResponse
     * @throws RouteNotFoundException
     * @throws MissingMandatoryParametersException
     * @throws InvalidParameterException
     * @throws \InvalidArgumentException
     * @throws OptimisticLockException
     * @throws ORMInvalidArgumentException
     */
    public function addAction($product): RedirectResponse
    {
        $this->orderManager->addOrder($product);

        return new RedirectResponse(
            $this->router->generate('orders')
        );
    }

    /**
     * @Route("/check-out", name="check_out")
     * @Template()
     * @Method("GET")
     * @Security("has_role('ROLE_USER')")
     * @param Request $request
     * @return Order[]
     * @throws \LogicException
     */
    public function checkOutAction(Request $request)
    {
        $page = $request->query->get('page') ?: 1;
        $pagination = $this->orderManager->findReadyForCheckOut($page);

        return [
            'pagination' => $pagination
        ];
    }

    /**
     * @Route("/order/end/{id}", name="order_end")
     * @Method({"GET"})
     * @ParamConverter("order", class="UmamiNationBundle\Entity\Order")
     * @param Order $order
     * @return RedirectResponse
     * @throws RouteNotFoundException
     * @throws MissingMandatoryParametersException
     * @throws InvalidParameterException
     * @throws ORMInvalidArgumentException
     * @throws OptimisticLockException
     * @throws \InvalidArgumentException
     */
    public function endAction($order)
    {
        $this->orderManager->end($order);

        return new RedirectResponse(
            $this->router->generate('orders')
        );
    }
}
