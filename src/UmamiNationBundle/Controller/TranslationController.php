<?php
namespace UmamiNationBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use UmamiNationBundle\Entity\User;
use UmamiNationBundle\User\UserFactory;
use UmamiNationBundle\User\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\Dumper\FileDumper;
use Symfony\Component\Translation\Dumper\JsonFileDumper;
use Symfony\Component\Translation\Translator;

/**
 * Class TranslationController
 * @package ERAMON\Portal\EramonPortalBundle\Controller
 * @Route(service="umami-nation.controller.translation")
 */
class TranslationController
{
    /** @var JsonFileDumper */
    private $dumper;
    /** @var RouterInterface */
    private $router;
    /** @var UserManager */
    private $userManager;
    /** @var UserFactory */
    private $userFactory;
    /** @var Translator */
    private $translator;
    /** @var  array */
    private $allLanguages;

    /**
     * @param FileDumper|JsonFileDumper $fileDumper
     * @param RouterInterface $router
     * @param UserManager $userManager
     * @param UserFactory $userFactory
     * @param Translator $translator
     * @param $allLanguages
     */
    public function __construct(
        FileDumper $fileDumper,
        RouterInterface $router,
        UserManager $userManager,
        UserFactory $userFactory,
        Translator $translator,
        $allLanguages
    ) {
        $this->dumper = $fileDumper;
        $this->router = $router;
        $this->userManager = $userManager;
        $this->userFactory = $userFactory;
        $this->translator = $translator;
        $this->allLanguages = $allLanguages;
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Symfony\Component\Routing\Exception\ResourceNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MethodNotAllowedException
     * @throws \InvalidArgumentException
     * @Method({"POST"})
     *
     * @Route("/changelanguage", name="change_language")
     */
    public function changeLanguageAction(Request $request): RedirectResponse
    {
        $newLanguage = $request->request->get('lang');
        $origin = \str_replace('/', '\/', $request->headers->get('host'));
        $referer = $request->headers->get('referer');

        $request->setLocale($newLanguage);
        $request->getSession()->set('_locale', $newLanguage);

        $this->router->getContext()->setParameter('_locale', $newLanguage);

        if ($this->userFactory->getUser() instanceof User) {
            $this->userManager->updateLanguage($this->userFactory->getUser(), $newLanguage);
        }

        $base = '/(?<=' . $origin . ')([^\?]*)/';
        \preg_match($base, $referer, $result);

        $this->router->getContext()->setMethod('GET');
        $previousUrl = \preg_match('/\/$|dashboard/', $result[1]) ?
            $this->router->generate('homepage') :
            $this->router->generate($this->router->match($result[1])['_route']);

        return new RedirectResponse($previousUrl);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     * @throws \InvalidArgumentException
     * @Method("GET")
     *
     * @Route("/translations", name="translations", defaults={"_format":"json"})
     */
    public function getTranslationsAction(Request $request): Response
    {
        $locale = $request->getLocale();

        $catalogue = $this->translator->getCatalogue($locale);
        $jsonTranslations = $this->dumper->formatCatalogue($catalogue, 'messages');

        return new Response($jsonTranslations);
    }

    /**
     * @Template()
     * @return array
     */
    public function languageSelectorAction(): array
    {
        return [
            'all_languages' => $this->allLanguages
        ];
    }
}
