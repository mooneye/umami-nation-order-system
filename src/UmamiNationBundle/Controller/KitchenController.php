<?php

namespace UmamiNationBundle\Controller;

use UmamiNationBundle\Entity\Order;
use UmamiNationBundle\Kitchen\Kitchenmanager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * @Route(service="umami-nation.controller.kitchen")
 */
class KitchenController
{
    /**
     * KitchenController constructor.
     * @param Router $router
     * @param KitchenManager $kitchenManager
     */
    public function __construct(Router $router, KitchenManager $kitchenManager)
    {
        $this->router = $router;
        $this->kitchenManager = $kitchenManager;
    }

    /**
     * @Route("/kitchen", name="kitchen")
     * @Template()
     * @Method("GET")
     * @Security("has_role('ROLE_USER')")
     * @param Request $request
     * @return Order[]
     */
    public function indexAction(Request $request)
    {
        $page = $request->query->get('page') ?: 1;
        $pagination = $this->kitchenManager->findOpenOrders($page);

        return [
            'pagination' => $pagination
        ];
    }

    /**
     * @Route("/kitchen/start/{id}", name="kitchen_start")
     * @Method({"GET"})
     * @ParamConverter("order", class="UmamiNationBundle\Entity\Order")
     * @param Order $order
     * @return RedirectResponse
     * @throws RouteNotFoundException
     * @throws MissingMandatoryParametersException
     * @throws InvalidParameterException
     * @throws ORMInvalidArgumentException
     * @throws OptimisticLockException
     * @throws \InvalidArgumentException
     */
    public function startAction(Order $order)
    {
        $this->kitchenManager->start($order);

        return new RedirectResponse(
            $this->router->generate('kitchen')
        );
    }

    /**
     * @Route("/kitchen/running", name="kitchen_running")
     * @Template()
     * @Method("GET")
     * @Security("has_role('ROLE_USER')")
     * @param Request $request
     * @return Order[]
     * @throws \LogicException
     */
    public function runAction(Request $request)
    {
        $page = $request->query->get('page') ?: 1;
        $pagination = $this->kitchenManager->findOrdersRunning($page);

        return [
            'pagination' => $pagination
        ];
    }

    /**
     * @Route("/kitchen/end/{id}", name="kitchen_end")
     * @Method({"GET"})
     * @ParamConverter("order", class="UmamiNationBundle\Entity\Order")
     * @param Order $order
     * @return RedirectResponse
     * @throws RouteNotFoundException
     * @throws MissingMandatoryParametersException
     * @throws InvalidParameterException
     * @throws ORMInvalidArgumentException
     * @throws OptimisticLockException
     * @throws \InvalidArgumentException
     */
    public function endAction($order)
    {
        $this->kitchenManager->end($order);

        return new RedirectResponse(
            $this->router->generate('kitchen')
        );
    }
}
