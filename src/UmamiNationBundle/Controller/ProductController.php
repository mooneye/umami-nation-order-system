<?php

namespace UmamiNationBundle\Controller;

use UmamiNationBundle\Entity\Product;
use UmamiNationBundle\Form\Product\DeleteProduct;
use UmamiNationBundle\Form\Product\SearchProduct;
use UmamiNationBundle\Form\Product\NewProduct;
use UmamiNationBundle\Product\ProductManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Config\Definition\Exception\DuplicateKeyException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;

/**
 * @Route(service="umami-nation.controller.product")
 */
class ProductController
{
    /** @var ProductManager $productManager */
    private $productManager;
    /** @var  FormFactoryInterface */
    private $formFactory;
    /** @var Router  */
    private $router;

    public function __construct(ProductManager $manager, FormFactoryInterface $formFactory, Router $router)
    {
        $this->productManager = $manager;
        $this->formFactory = $formFactory;
        $this->router = $router;
    }

    /**
     * @Route("/products", name="products")
     * @Template()
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return array
     * @throws \UnexpectedValueException
     * @throws InvalidOptionsException
     */
    public function indexAction(Request $request): array
    {
        $page = $request->query->get('page') ?: 1;
        $searchForm = $this->createSearchForm();
        $searchForm->handleRequest($request);

        $pagination = [];
        $formSubmitted = $searchForm->isSubmitted();
        if ($formSubmitted && $searchForm->isValid()) {
            $pagination = $this->productManager->search($searchForm->getData(), $page);
        }

        return [
            'pagination' => $pagination,
            'formSubmitted' => $formSubmitted,
            'search_form' => $searchForm->createView()
        ];
    }

    /**
     * @Route("/product/new", name="product_new")
     * @Method({"GET", "POST"})
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return array|RedirectResponse
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \InvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws InvalidOptionsException
     */
    public function newAction(Request $request)
    {
        $product = $this->productManager->createNew();
        $newForm = $this->createNewForm($product);
        $newForm->handleRequest($request);

        if ($newForm->isSubmitted() && $newForm->isValid()) {
            try {
                $this->productManager->save($product);
            } catch (DuplicateKeyException $e) {
                return [
                    'create_form' => $newForm->createView(),
                    'duplicateKey' => true
                ];
            }

            return new RedirectResponse(
                $this->router->generate('product_show', ['id' => $product->getId()])
            );
        }

        return [
            'create_form' => $newForm->createView()
        ];
    }

    /**
     * @Route("/product/{id}", name="product_show")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     * @param Product $product
     * @return array
     * @throws InvalidOptionsException
     */
    public function showAction(Product $product): array
    {
        return [
            'product' => $product
        ];
    }

    /**
     * @Route("/product/edit/{id}", name="product_edit")
     * @Method({"GET", "POST"})
     * @Template()
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param Product $product
     * @return array|RedirectResponse
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \InvalidArgumentException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Symfony\Component\OptionsResolver\Exception\InvalidOptionsException
     */
    public function editAction(Request $request, Product $product)
    {
        $editForm = $this->createNewForm($product);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->productManager->persist($product);

            return new RedirectResponse(
                $this->router->generate('product_show', ['id' => $product->getId()])
            );
        }

        return [
            'product' => $product,
            'edit_form' => $editForm->createView()
        ];
    }

    /**
     * @Route("/product/delete/{id}", name="product_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Product $product
     * @return RedirectResponse
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \InvalidArgumentException
     */
    public function deleteAction(Request $request, Product $product): RedirectResponse
    {
        $this->productManager->delete($product);

        return new RedirectResponse(
            $this->router->generate('products')
        );
    }

    /**
     * @return FormInterface
     * @throws InvalidOptionsException
     */
    private function createSearchForm(): FormInterface
    {
        return $this->formFactory->create(
            SearchProduct::class
        );
    }

    /**
     * @param Product $product
     * @return FormInterface
     * @throws InvalidOptionsException
     */
    private function createNewForm($product): FormInterface
    {
        return $this->formFactory->create(
            NewProduct::class,
            $product
        );
    }
}
