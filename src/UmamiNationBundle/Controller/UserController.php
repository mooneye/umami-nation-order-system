<?php

namespace UmamiNationBundle\Controller;

use Symfony\Component\Form\Exception\AlreadySubmittedException;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use UmamiNationBundle\Entity\User;
use UmamiNationBundle\Form\User\SearchUser;
use UmamiNationBundle\Form\User\UpdateUser;
use UmamiNationBundle\User\LoginUserFactory;
use UmamiNationBundle\User\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route(service="umami-nation.controller.user")
 */
class UserController
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;
    /**
     * @var LoginUserFactory
     */
    private $loginUserFactory;

    /**
     * @param FormFactoryInterface $formFactory
     * @param UserManager $userManager
     * @param UrlGeneratorInterface $router
     * @param LoginUserFactory $loginUserFactory
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        UserManager $userManager,
        UrlGeneratorInterface $router,
        LoginUserFactory $loginUserFactory
    ) {
        $this->formFactory = $formFactory;
        $this->userManager = $userManager;
        $this->router = $router;
        $this->loginUserFactory = $loginUserFactory;
    }

    /**
     * @Route("/users", name="user_administration")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     * @Method("GET")
     * @param Request $request
     * @return array
     * @throws InvalidOptionsException
     */
    public function indexAction(Request $request): array
    {
        $page = $request->query->get('page') ?: 1;
        $searchForm = $this->createSearchForm();
        $searchForm->handleRequest($request);

        $pagination = [];
        $formSubmitted = $searchForm->isSubmitted();
        if ($formSubmitted && $searchForm->isValid()) {
            $pagination = $this->userManager->search($searchForm->getData(), $page);
        }

        return [
            'pagination' => $pagination,
            'formSubmitted' => $formSubmitted,
            'search_form' => $searchForm->createView()
        ];
    }

    /**
     * @Route("/user/{username}", name="user_show")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     *
     * @param User $user
     * @param Request $request
     * @return array|Response
     * @throws RouteNotFoundException
     * @throws MissingMandatoryParametersException
     * @throws InvalidParameterException
     * @throws \InvalidArgumentException
     * @throws AlreadySubmittedException
     * @throws InvalidOptionsException
     */
    public function showAction(User $user, Request $request)
    {
        $user->setPassword('');
        return [
            'user' => $user
        ];
    }

    /**
     * @Route("/user/edit/{username}", name="user_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     *
     * @param User $user
     * @param Request $request
     * @return array|Response
     * @throws AccessDeniedException
     * @throws RouteNotFoundException
     * @throws MissingMandatoryParametersException
     * @throws InvalidParameterException
     * @throws \InvalidArgumentException
     * @throws AlreadySubmittedException
     * @throws InvalidOptionsException
     */
    public function editAction(User $user, Request $request)
    {
        $this->checkIfMayEditUser($user);

        $editForm = $this->createUpdateForm($user);
        $editForm->remove('password');
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->userManager->save($user);
            return new RedirectResponse($this->router->generate('user_administration'));
        }

        return ['user' => $user, 'edit_form' => $editForm->createView()];
    }

    /**
     * @Route("/user/new", name="user_new")
     * @Template()
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @param Request $request
     * @return array|Response
     * @throws RouteNotFoundException
     * @throws MissingMandatoryParametersException
     * @throws InvalidParameterException
     * @throws \InvalidArgumentException
     * @throws InvalidOptionsException
     */
    public function newAction(Request $request)
    {
        $user = $this->userManager->createNew();
        $newForm = $this->createUpdateForm($user);
        $newForm->handleRequest($request);

        if ($newForm->isSubmitted() && $newForm->isValid()) {
            try {
                $this->userManager->save($user);
            } catch (DuplicateKeyException $e) {
                return [
                    'create_form' => $newForm->createView(),
                    'duplicateKey' => true
                ];
            }

            return new RedirectResponse(
                $this->router->generate('user_show', ['username' => $user->getUsername()])
            );
        }

        return ['create_form' => $newForm->createView()];
    }

    /**
     * @Route("/users/delete/{id}", name="user_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     * @throws MissingMandatoryParametersException
     * @throws InvalidParameterException
     * @throws \InvalidArgumentException
     * @throws AccessDeniedException
     */
    public function deleteAction(Request $request, User $user): RedirectResponse
    {
        $this->checkIfMayEditUser($user);
        $this->userManager->remove($user);

        return new RedirectResponse(
            $this->router->generate('user_administration')
        );
    }

    /**
     * @Template()
     *
     * @Route("/changePassword", name="change_own_password")
     * @ParamConverter("validatePassword", options={"type"="User\ValidatePassword"})
     * @ParamConverter("changePassword", options={"type"="User\ChangePassword"})
     *
     * @param FormInterface $validatePassword
     * @param FormInterface $changePassword
     * @return array
     */
    public function changeOwnPasswordAction(FormInterface $validatePassword, FormInterface $changePassword): array
    {
        if ($changePassword->isSubmitted() && $changePassword->isValid()) {
            $currentUser = $this->loginUserFactory->getUser();

            $changePasswordData = $changePassword->getData();
            $this->userManager->updatePassword($currentUser, $changePasswordData['password']);

            return [
                'passwordUpdated' => true
            ];
        }

        if ($validatePassword->isSubmitted()) {
            $validateData = $validatePassword->getData();
            $currentUser = $this->loginUserFactory->getUser();

            if ($this->userManager->checkIfPasswordIsValid($currentUser, $validateData['password'])) {
                return [
                    'isValid' => true,
                    'changePassword' => $changePassword->createView()
                ];
            }

            return [
                'isValid' => false,
                'validatePassword' => $validatePassword->createView()
            ];
        }

        return [
            'validatePassword' => $validatePassword->createView()
        ];
    }

    /**
     * @Template()
     * @Route("/changeProfile", name="change_own_profile")
     * @Method("GET|POST")
     *
     * @param Request $request
     * @return array
     * @throws InvalidOptionsException
     * @throws AlreadySubmittedException
     */
    public function changeOwnProfileAction(Request $request): array
    {
        $currentUser = $this->loginUserFactory->getUser();

        $updateUserForm = $this->formFactory->create(UpdateUser::class, $currentUser);
        $updateUserForm
            ->remove('password')
            ->remove('username')
            ->remove('email')
            ->remove('roles');

        $updateUserForm->handleRequest($request);
        if ($updateUserForm->isValid()) {
            $this->userManager->save($currentUser);
        }

        return [
            'editForm' => $updateUserForm->createView()
        ];
    }

    /**
     * @param User $user
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function checkIfMayEditUser(User $user)
    {
        $editSuperAdmin = \in_array('ROLE_SUPER_ADMIN', $user->getRoles(), true);
        $isSuperAdmin = \in_array('ROLE_SUPER_ADMIN', $this->loginUserFactory->getUser()->getRoles(), true);

        if ($editSuperAdmin && !$isSuperAdmin) {
            throw new AccessDeniedException(
                'Customer admins may not edit Superadmins'
            );
        }
    }

    /**
     * @return FormInterface
     * @throws InvalidOptionsException
     */
    private function createSearchForm(): FormInterface
    {
        return $this->formFactory->create(
            SearchUser::class
        );
    }

    /**
     * @param User $user
     * @return FormInterface
     * @throws InvalidOptionsException
     */
    private function createUpdateForm(User $user): FormInterface
    {
        return $this->formFactory->create(
            UpdateUser::class,
            $user
        );
    }
}
