<?php

namespace UmamiNationBundle\Product;

use UmamiNationBundle\Entity\Product;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Symfony\Component\Config\Definition\Exception\DuplicateKeyException;

class ProductManager
{
    /** @var EntityManager */
    private $entityManager;
    /** @var Paginator */
    private $knpPaginator;

    /**
     * @param EntityManager $entityManager
     * @param Paginator $knpPaginator
     */
    public function __construct(
        EntityManager $entityManager,
        Paginator $knpPaginator
    ) {
        $this->entityManager = $entityManager;
        $this->knpPaginator = $knpPaginator;
    }

    /**
     * @param array $criteria
     *
     * @return Product[]
     * @throws \UnexpectedValueException
     */
    public function search(array $criteria, $currentPage)
    {
        $name = $criteria['name'] ?: '%';
        $price = $criteria['price'] ?: 0;
        $qb = $this->entityManager->getRepository(Product::class)->createQueryBuilder('p');
        $query = $qb->select()
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->like('p.name', ':name'),
                    $qb->expr()->gte('p.price', ':price')
                )
            )
            ->setParameters([
                'name' => $name,
                'price' => $price
            ])
            ->orderBy('p.name', 'ASC')
            ->getQuery();

        return $this->paginate($query, $currentPage);
    }

    /**
     * @return Product[]
     */
    public function findAll()
    {
        return $this->entityManager->getRepository(Product::class)->findAll();
    }

    /**
     * @return Product
     */
    public function createNew(): Product
    {
        $product = new Product();
        $product->updatedTimestamps();

        return $product;
    }

    /**
     * @param Product $product
     * @throws DuplicateKeyException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Product $product)
    {
        if ($this->hasDuplicateEntry($product)) {
            throw new DuplicateKeyException(
                "The product '" . $product->getName() . "' already exists."
            );
        }
        $this->persist($product);
    }

    /**
     * @param Product $product
     * @return bool
     */
    private function hasDuplicateEntry(Product $product): bool
    {
        $result = $this->entityManager->getRepository(Product::class)->findBy(
            [
                'name' => $product->getName()
            ]
        );

        return \count($result) > 0;
    }

    /**
     * @param Product $product
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist(Product $product)
    {
        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }

    /**
     * @param Product $product
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete($product)
    {
        $this->entityManager->remove($product);
        $this->entityManager->flush();
    }

    /**
     * @param $query
     * @param $page
     * @return PaginationInterface
     * @throws \LogicException
     */
    private function paginate($query, $page)
    {
        return $this->knpPaginator->paginate(
            $query,
            $page
        );
    }
}
