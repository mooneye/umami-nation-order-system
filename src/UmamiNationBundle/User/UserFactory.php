<?php
namespace UmamiNationBundle\User;

use UmamiNationBundle\Entity\User;

interface UserFactory
{
    /**
     * @return User
     */
    public function getUser(): User;
}
