<?php
namespace UmamiNationBundle\User;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
//use UmamiNationBundle\Entity\Customer;
use UmamiNationBundle\Entity\User;

/**
 * @package UmamiNationBundle\User
 */
class UserRepository extends EntityRepository
{
    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $class = new ClassMetadata(User::class);

        parent::__construct($em, $class);
    }

    /**
     * @param int $customerId
     * @return User[]
     */
    public function getUsersByCustomerId($customerId): array
    {
        return $this->findBy(['customerId' => $customerId]);
    }

    /**
     * @return array
     */
    public function getCustomerIdList(): array
    {
        $queryBuilder = $this->createQueryBuilder('u')
            ->select('u.customerId')
            ->distinct();

        $uniqueCustomers = \array_map(
            function ($customer) {
                return $customer['customerId'];
            },
            $queryBuilder->getQuery()->getResult()
        );

        return $uniqueCustomers;
    }

    /**
     * @param $customers
     * @return array
     */
    public function getAllUsersGroupedByCustomers($customers): array
    {
        /** @var User[] $allUsers */
        $allUsers = $this->findAll();
        return $this->groupUsersByCustomers($allUsers, $customers);
    }

    /**
     * @param User[] $allUsers
     * @param $customers
     * @return array
     */
    private function groupUsersByCustomers($allUsers, $customers): array
    {
        /*$groupedUsers = [];
        foreach ($allUsers as $user) {
            if ($user->getCustomerId()) {
                $customerId = $user->getCustomerId();
                $activeCustomer = array_filter(
                    $customers,
                    function (Customer $customer) use ($customerId) {
                        return $customer->getId() == $customerId;
                    }
                );

                $activeCustomer = array_shift($activeCustomer);

                $groupedUsers[$activeCustomer->getName()][] = $user;
            } else {
                $groupedUsers['Any Customer'][] = $user;
            }
        }

        return $groupedUsers;*/
        return [];
    }
}
