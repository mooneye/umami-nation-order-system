<?php

namespace UmamiNationBundle\User;

use UmamiNationBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use InvalidArgumentException;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class UserManager
{
    const RESULTS_PER_PAGE = 10;
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var UserPasswordEncoder */
    private $passwordEncoder;
    /** @var string[] */
    private $allLanguages;
    /** @var Paginator */
    private $knpPaginator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoder $passwordEncoder
     * @param array|string[] $allLanguages
     * @param $knpPaginator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoder $passwordEncoder,
        array $allLanguages,
        Paginator $knpPaginator
    ) {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->allLanguages = $allLanguages;
        $this->knpPaginator = $knpPaginator;
    }

    /**
     * @param User $user
     * @param string $newLocale
     * @throws \InvalidArgumentException
     */
    public function updateLanguage(User $user, $newLocale)
    {
        if (!\array_key_exists($newLocale, $this->allLanguages)) {
            throw new InvalidArgumentException('Unknown language: ' . $newLocale);
        }
        $user->setLocale($newLocale);
        $this->entityManager->persist($user);
    }
    /**
     * @param User $user
     * @return string
     */
    private function getEncodedPassword(User $user): string
    {
        return $this->passwordEncoder->encodePassword($user, $user->getPassword());
    }

    /**
     * @param array $criteria
     *
     * @return array|User[]
     * @throws \UnexpectedValueException
     */
    public function findBy(array $criteria)
    {
        return $this->entityManager->getRepository(User::class)->findBy($criteria);
    }
    /**
     * @return array|User[]
     */
    public function findAll()
    {
        return $this->entityManager->getRepository(User::class)->findAll();
    }
    /**
     * @param User $user
     */
    public function create(User $user)
    {
        $user->setPassword($this->getEncodedPassword($user));
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
    /**
     * @param User $user
     */
    public function save(User $user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
    /**
     * @param User $user
     */
    public function remove(User $user)
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }
    /**
     * @param User $user
     * @param $password
     * @return bool
     */
    public function checkIfPasswordIsValid(User $user, $password): bool
    {
        return $this->passwordEncoder->isPasswordValid(
            $user,
            $password
        );
    }
    /**
     * @param User $user
     * @param string $password
     */
    public function updatePassword(User $user, $password)
    {
        $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
        $this->save($user);
    }

    /**
     * @param array $criteria
     * @param int $currentPage
     * @return Paginator
     */
    public function search(array $criteria, $currentPage)
    {
        $name = $criteria['username'] ?: '%';
        $email = $criteria['email'] ?: '%';
        $isActive = $criteria['isActive'] ?: 0;
        $firstName = $criteria['firstName'] ?: '%';
        $lastName = $criteria['lastName'] ?: '%';
        $roles = $criteria['roles'] ?: '%';

        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(User::class)->createQueryBuilder('u');
        $query = $qb->select()
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->like('u.username', ':name'),
                    $qb->expr()->like('u.email', ':email'),
                    $qb->expr()->gte('u.isActive', ':isActive'),
                    $qb->expr()->like('u.firstName', ':firstName'),
                    $qb->expr()->like('u.lastName', ':lastName'),
                    $qb->expr()->like('u.roles', ':roles')
                )
            )
            ->setParameters([
                'name' => $name,
                'email' => $email,
                'isActive' => $isActive,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'roles' => $roles
            ])
            ->orderBy('u.username', 'ASC')
            ->getQuery();

        return $this->paginate($query, $currentPage);
    }

    /**
     * @param $query
     * @param $page
     * @return PaginationInterface
     * @throws \LogicException
     */
    private function paginate($query, $page)
    {
        return $this->knpPaginator->paginate(
            $query,
            $page
        );
    }

    /**
     * @return User
     */
    public function createNew()
    {
        $user = new User();
        $user->setIsActive(true);

        return $user;
    }
}
