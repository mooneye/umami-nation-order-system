<?php

namespace UmamiNationBundle\Archive;

use UmamiNationBundle\Entity\Order;
use UmamiNationBundle\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;

/**
 * Class ArchiveManager
 * @package UmamiNationBundle\Archive
 */
class ArchiveManager
{
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var Paginator */
    private $knpPaginator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param Paginator $knpPaginator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Paginator $knpPaginator
    ) {
        $this->entityManager = $entityManager;
        $this->knpPaginator = $knpPaginator;
    }

    /**
     * @param array $criteria
     * @param $currentPage
     * @return PaginationInterface
     * @throws \LogicException
     */
    public function search(array $criteria, $currentPage)
    {
        /** @var Product $product */
        $product = $criteria['product'] ?: null;
        /** @var \DateTime $orderEnd */
        $orderEnd = $criteria['orderEnd'] ?: new \DateTime('2016-01-01 00:00:00');
        if ($product !== null) {
            $query = $this->searchByProduct($product, $orderEnd);
        } else {
            $query = $this->findByDate($orderEnd);
        }

        return $this->paginate($query, $currentPage);
    }

    /**
     * @param $query
     * @param $page
     * @return PaginationInterface
     * @throws \LogicException
     */
    private function paginate($query, $page)
    {
        return $this->knpPaginator->paginate(
            $query,
            $page
        );
    }

    /**
     * @param Product $product
     * @param \DateTime $orderEnd
     * @return \Doctrine\ORM\Query
     */
    private function searchByProduct(Product $product, \DateTime $orderEnd)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Order::class)->createQueryBuilder('o');
        return $qb->select()
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->isNotNull('o.orderEnd'),
                    $qb->expr()->gte('o.orderEnd', ':orderEnd'),
                    $qb->expr()->eq('o.product', ':productId')
                )
            )
            ->setParameters([
                'productId' => $product->getId(),
                'orderEnd' => $orderEnd->format('Y-m-d H:i:s')
            ])
            ->orderBy('o.orderEnd', 'DESC')
            ->getQuery();
    }

    /**
     * @param \DateTime $orderEnd
     * @return \Doctrine\ORM\Query
     */
    private function findByDate(\DateTime $orderEnd)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Order::class)->createQueryBuilder('o');
        return $qb->select()
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->isNotNull('o.orderEnd'),
                    $qb->expr()->gte('o.orderEnd', ':orderEnd')
                )
            )
            ->setParameters([
                'orderEnd' => $orderEnd->format('Y-m-d H:i:s')
            ])
            ->orderBy('o.orderEnd', 'DESC')
            ->getQuery();
    }
}
