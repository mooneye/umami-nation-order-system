<?php

namespace UmamiNationBundle\Order;

use UmamiNationBundle\Entity\Order;
use UmamiNationBundle\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;

class OrderManager
{
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var Paginator */
    private $knpPaginator;

    /**
     * @param EntityManagerInterface $entityManager
     * @param Paginator $knpPaginator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Paginator $knpPaginator
    ) {
        $this->entityManager = $entityManager;
        $this->knpPaginator = $knpPaginator;
    }

    /**
     * @param array $criteria
     *
     * @return array|Product[]
     * @throws \UnexpectedValueException
     */
    public function findBy(array $criteria)
    {
        return $this->entityManager->getRepository(Product::class)->findBy($criteria);
    }
    /**
     * @return array|Product[]
     */
    public function findAll()
    {
        return $this->entityManager->getRepository(Product::class)->findAll();
    }

    /**
     * @param Product $product
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addOrder(Product $product)
    {
        $this->save(
            $this->createNew($product)
        );
    }

    /**
     * @param Product $product
     * @return Order
     */
    public function createNew(Product $product)
    {
        $order = new Order();
        $order->setProduct($product);
        $order->setOrderStart(new \DateTime('now'));

        return $order;
    }

    /**
     * @param Order $order
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Order $order)
    {
        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    /**
     * @param $currentPage
     * @return PaginationInterface
     * @throws \LogicException
     */
    public function findReadyForCheckOut($currentPage)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Order::class)->createQueryBuilder('o');
        $query = $qb->select('o')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->isNotNull('o.kitchenStart'),
                    $qb->expr()->isNotNull('o.kitchenEnd'),
                    $qb->expr()->isNull('o.orderEnd')
                )
            )
            ->orderBy('o.kitchenEnd', 'ASC')
            ->getQuery();

        return $this->paginate($query, $currentPage);
    }

    /**
     * @param $query
     * @param $page
     * @return PaginationInterface
     * @throws \LogicException
     */
    private function paginate($query, $page)
    {
        return $this->knpPaginator->paginate(
            $query,
            $page
        );
    }

    /**
     * @param Order $order
     * @throws ORMInvalidArgumentException
     * @throws OptimisticLockException
     */
    public function end($order)
    {
        $order->setOrderEnd(new \DateTime('now'));
        $this->save($order);
    }
}
