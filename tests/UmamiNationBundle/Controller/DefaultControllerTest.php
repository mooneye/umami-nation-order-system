<?php

namespace Tests\UmamiNationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest
 * @package Tests\UmamiNationBundle\Controller
 */
class DefaultControllerTest extends WebTestCase
{
    /**
     *
     */
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/de');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}
