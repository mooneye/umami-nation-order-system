const plugins = {
  'gulpUtil': require('gulp-util'),
  'NpmImport': require('less-plugin-npm-import')
};

const functions = {
  'chug': require('gulp-chug'),
  'exec': require('gulp-exec'),
  'gulpif': require('gulp-if'),
  'gzip': require('gulp-gzip'),
  'render': {
    'concat': require('gulp-concat'),
    'less': require('gulp-less'),
    'sourcemaps': require('gulp-sourcemaps')
  },
  'tar': require('gulp-tar'),
  'uglify': {
    'css': require('gulp-uglifycss'),
    'js': require('gulp-uglify')
  }
};

const repositoryPath = process.env.PWD;
const bundleName = 'UmamiNationBundle';
const paths = {
  'bundlePath': `${repositoryPath}/web/bundles/umamination`,
  'nodeModules': `${repositoryPath}/node_modules`,
  'publicResources': `./src/${bundleName}/Resources/public`,
  'resources': `./src/${bundleName}/Resources`,
  'webPath': `${repositoryPath}/web`
};

const styles = {
  'bootstrapSwitch': `${paths.nodeModules}/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css`,
  'dateTimePicker': `${paths.nodeModules}/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css`,
  'intern': `${paths.resources}/styles/less/master.less`
};

const jsDependencies = {
  'jquery': `${paths.nodeModules}/jquery/dist/jquery.min.js`,
  'moment': `${paths.nodeModules}/moment/min/moment.min.js`,
  'bootstrap': `${paths.nodeModules}/bootstrap/dist/js/bootstrap.min.js`,
  'bootstrapSwitch': `${paths.nodeModules}/bootstrap-switch/dist/js/bootstrap-switch.js`,
  'dateTimePicker': `${paths.nodeModules}/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js`,
  'passwordMeter': `${paths.nodeModules}/zxcvbn/dist/zxcvbn.js`,
  'passwordMeterBootstrap': `${paths.nodeModules}/pwstrength-bootstrap/dist/pwstrength-bootstrap.min.js`
};

const options = {
  'gulpEnv': process.env.GULP_ENV,
  'lessCompileOptions': {'plugins': [new plugins.NpmImport({'prefix': '~'})]},
  'reportOptions': {
    // Default = true, false means don't write err
    'err': true,
    // Default = true, false means don't write stderr
    'stderr': true,
    // Default = true, false means don't write stdout
    'stdout': true
  },
  'runOptions': {
    // Default = false, true means don't emit error event
    'continueOnError': true,
    // Default = false, true means stdout is written to file.contents
    'pipeStdout': false
  }
};

const packageConfig = require('./package.json');
const packageName = `${packageConfig.name}.tar`;
const files = [
  'app/**',
  '!app/config/parameters.yml',
  'src/**',
  'vendor/**',
  'web/**',
  'web/.htaccess',
  '!web/app_dev.php',
  'bin/console'
];

module.exports = {
  files,
  functions,
  jsDependencies,
  options,
  packageName,
  paths,
  plugins,
  repositoryPath,
  styles
};
