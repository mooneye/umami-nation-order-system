/* **************************************************************************************
 * CONSTANTS
 **************************************************************************************/
const gulp = require('gulp');
const runSequence = require('run-sequence');
const {
  files,
  functions,
  jsDependencies,
  options,
  packageName,
  paths,
  plugins,
  repositoryPath,
  styles
} = require('./gulpConfig');

/* **************************************************************************************
 * GENERAL
 **************************************************************************************/
gulp.task('release', (done) => {
  runSequence('build', 'tarball', () => {
    done();
  });
});
gulp.task('build', (done) => {
  runSequence('default', () => {
    done();
  });
});

gulp.task('default', (done) => {
  runSequence('install:assets', 'render:less', () => {
    done();
  });
});

/* **************************************************************************************
 * INSTALLS
 **************************************************************************************/
gulp.task('install:assets',
  [
    'install:bootstrap',
    'install:fonts',
    'install:icons',
    'install:javascript',
    'render:javascript',
    'render:images'
  ]
);

gulp.task('install:bootstrap', () => {
  return gulp.src([`${paths.nodeModules}/bootstrap/less/**/*`])
    .pipe(gulp.dest(`${paths.resources}/styles/bootstrap`));
});
gulp.task('install:fonts', () => {
  return gulp.src([`${paths.nodeModules}/bootstrap/fonts/*`])
    .pipe(gulp.dest(`${paths.bundlePath}/fonts`));
});

gulp.task('install:icons', () => {
  return gulp.src([`${paths.publicResources}/icons/*.ico`])
    .pipe(gulp.dest(`${paths.webPath}/`));
});

gulp.task('install:javascript', () => {
  const arr = Object.keys(jsDependencies).map((key) => jsDependencies[key]);

  return gulp.src(arr)
    .pipe(functions.render.concat('vendors.js'))
    .pipe(functions.gulpif(options.gulpEnv === 'prod', functions.uglify.js()))
    .pipe(gulp.dest(`${paths.bundlePath}/js`));
});

/**
 *  *************************************************************************************
 * WATCH
 **************************************************************************************/
gulp.task('watch:assets',
  [
    'watch:styles',
    'watch:javascript',
    'watch:images'
  ]
);
// ------------------------- STYLES -----------------------------
gulp.task('watch:styles', () => {
  const onChange = (event) => {
    plugins.gulpUtil.log(plugins.gulpUtil.colors.magenta('File', event.type, ' at ', event.path));
  };
  gulp.watch(`${paths.resources}/styles/less/**/*.less`, ['render:less'])
    .on('change', onChange);
});

gulp.task('render:less', () => {
  const arr = Object.keys(styles).map((key) => styles[key]);

  return gulp.src(arr)
    .pipe(functions.gulpif(/[.]less/, functions.render.less(options.lessCompileOptions)))
    .pipe(functions.render.sourcemaps.init())
    .pipe(functions.render.concat('styles.css'))
    .pipe(functions.gulpif(options.gulpEnv === 'prod', functions.uglify.css()))
    .pipe(functions.render.sourcemaps.write('./'))
    .pipe(gulp.dest(`${paths.bundlePath}/css`));
});

// ------------------------- JAVASCRIPT -----------------------------
gulp.task('watch:javascript', () => {
  const onChange = (event) => {
    plugins.gulpUtil.log(plugins.gulpUtil.colors.magenta('File', event.type, ' at ', event.path));
  };
  gulp.watch(`${paths.resources}/js/**/*.js`, ['render:javascript'])
    .on('change', onChange);
});

gulp.task('render:javascript', () => {
  return gulp.src([`${paths.resources}/js/**/*.js`])
    .pipe(functions.render.sourcemaps.init())
    .pipe(functions.render.concat('main.js'))
    .pipe(functions.uglify.js())
    .pipe(functions.render.sourcemaps.write())
    .pipe(gulp.dest(`${paths.bundlePath}/js`));
});

// ------------------------ IMAGES ----------------------------------
gulp.task('watch:images', () => {
  const onChange = (event) => {
    plugins.gulpUtil.log(plugins.gulpUtil.colors.magenta('File', event.type, ' at ', event.path));
  };
  gulp.watch(`${paths.publicResources}/img/**/*`, ['render:images'])
    .on('change', onChange);
});

gulp.task('render:images', () => {
  return gulp.src([`${paths.publicResources}/img/**/*`])
    .pipe(gulp.dest(`${paths.bundlePath}/img`));
});

/* **************************************************************************************
 * DEPLOYMENT
 **************************************************************************************/
gulp.task('tarball', () => {
  plugins.gulpUtil.log(plugins.gulpUtil.colors.magenta('Started packaging a tarball.'));
  gulp.src(files, {
    'base': repositoryPath,
    'follow': true
  })
    .pipe(functions.tar(packageName))
    .pipe(functions.gzip())
    .pipe(gulp.dest(`${repositoryPath}/build`));

  plugins.gulpUtil.log(plugins.gulpUtil.colors.magenta(`Saved tarball using the name ${packageName}`));
});
