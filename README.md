#Just a little Symfony Demo for my own

Umami-Nation Order System
=========================

Welcome to the `Umami-Nation Order System` Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

Requirements
--------------
* PHP >= 7.0
* MYSQL >= 5.7
* Composer
* nodeJS + npm
* gulp

What's inside?
--------------

The `Umami-Nation Order System` Standard Edition is configured with the following defaults:

  * An UmamiNationBundle you can use to start coding;

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

It comes pre-configured with the following bundles:

  * [**FrameworkBundle**][2] - The core Symfony framework bundle
  
  * [**WebServerBundle**][3]
  
  * [**KnpPaginatorBundle**][4]
  
  * [**SensioFrameworkExtraBundle**][5] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][6] - Adds support for the Doctrine ORM

  * [**TwigBundle**][7] - Adds support for the Twig templating engine

  * [**SecurityBundle**][8] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][9] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][10] - Adds support for Monolog, a logging library

  * [**SensioGeneratorBundle**][11] (in dev/test env) - Adds code generation
    capabilities
  
  * [**Bootstrap 3.7**][12] (in dev/test env) - Adds code generation


Install
--------------
[**Download Git-Bundle**][13]

Install gulp, nodeJS and npm and run:
```bash
[root@pc]# composer install
[root@pc]# npm run build
```

Start Local Server
```bash
[root@pc]# bin/console server:run
```

All libraries and bundles included in the `Umami-Nation Order System` Standard Edition are
released under the MIT or BSD license.


[1]:  https://symfony.com/doc/3.3/setup.html
[2]:  https://github.com/symfony/framework-bundle
[3]:  https://github.com/symfony/web-server-bundle
[4]:  https://github.com/KnpLabs/KnpPaginatorBundle
[5]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[6]:  https://symfony.com/doc/3.3/doctrine.html
[7]:  https://symfony.com/doc/3.3/templating.html
[8]:  https://symfony.com/doc/3.3/security.html
[9]:  https://symfony.com/doc/3.3/email.html
[10]: https://symfony.com/doc/3.3/logging.html
[11]: https://symfony.com/doc/current/bundles/SensioGeneratorBundle/index.html
[12]: http://getbootstrap.com/
[13]: https://github.com/sedlareg/symfony_demo.git
